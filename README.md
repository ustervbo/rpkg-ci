# rpkgci

<!-- badges: start -->
[![pipeline status](https://gitlab.com/ustervbo/rpkg-ci/badges/master/pipeline.svg)](https://gitlab.com/ustervbo/rpkg-ci/-/commits/master)
[![coverage report](https://gitlab.com/ustervbo/rpkg-ci/badges/master/coverage.svg)](https://gitlab.com/ustervbo/rpkg-ci/-/commits/master)
<!-- badges: end -->

The goal of `rpkgci` is to interactively get gitlab-ci to work

## Installation

This package is only available through this private gitlab repository.

## Example

This is a basic example which shows you how to solve a common problem - how to greet a place:

``` r
library(rpkgci)

say_this("Hello", "there")
```

## `.gitlab-ci.yml`

This package use `renv` to keep packages for the package development local and `devtools` which is just awesome for package development. `renv` is a replacement for `packrat` which should solve some of the problems.

### The most simple `.gitlab-ci.yml`

The most simple `.gitlab-ci.yml` for a package without `renv` is

```yaml
image: r-base:latest

check:
  script:
    - Rscript -e "install.packages('testthat')"
    - R CMD build . --no-build-vignettes --no-manual
    - R CMD check *tar.gz --no-build-vignettes --no-manual
```

If I wasn't using tests, I could have skipped the `- Rscript -e "install.packages('testthat')"` all together. To use the above `yml` with this package, you first need to comment out all the content of `./renv/activate.R` - otherwise `devtools` is automatically installed, and this will fail because of dependencies (see below for a more appropriate `yml` file).

However, the CI does nothing to report if the build is passing nor does it flaunt the large amount of work we have invested in documenting and testing the functions of the package.

One thing I love about the `devtools` package are the helpful functions that help you focus on solving problems with different aspect of the package development. For instance `devtools::check_man()` checks the documentation, `devtools::build_vignettes()` and `devtools::clean_vignettes()` build the package vignettes. `devtools::check()` does it all - it builds and checks a source package (basically the two `R CMD` lines in the most simple `.gitlab-ci.yml` above).

### A slightly more complex `.gitlab-ci.yml`

If the run fails with the reason "Build log exceeded limit of 4194304 bytes", go to Settings > CI/CD > Variables on the project on gitlab.com.

A large drawback to use `devtools` is the many dependencies. If we want to check the package using `devtools::check()` the `.gitlab-ci.yml` becomes slightly more complex, as we have to install some packages into the `r-base` docker:

```yaml
image: r-base:latest

stages:
  - check

before_script:
  - 'apt-get update'
  # Install needed packages. The call makes apt-get say yes to everything (-y),
  # and give no output except for errors (-qq)
  # We use the `check()` function of the devtools-package which imports the R-packages
  # curl - which need `libcurl4-openssl-dev` to be installed
  # openssl -  which need `libssl-dev` to be installed
  # xml2 - which need `libxml2-dev` to be installed
  - 'apt-get install -y -qq libcurl4-openssl-dev libssl-dev libxml2-dev'
  # Satisfy system dependencies for the R-packages
  # systemfonts (ggplot2 -> ragg -> systemfonts) needs libfontconfig1-dev
  # textshaping (ggplot2 -> ragg -> textshaping) needs libharfbuzz-dev libfribidi-dev
  # ragg needs libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
  # We need 'qpdf' for checks on pdfs
  - >
    apt-get install -y -qq
    libfontconfig1-dev
    libharfbuzz-dev libfribidi-dev
    libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
    qpdf

check:
  stage: check
  script:
    - Rscript -e "install.packages('devtools')"
    - Rscript -e "devtools::check()"
```

Alternatively, I'd just use the `rocker/tidyverse` instead of `r-base`, but please indulge my adventures into the didactic.

On Gitlab, the above yaml takes about 15 minutes to finish. By clever use of caches we can reduce this greatly.

### Using cache `.gitlab-ci.yml`

This version, takes just as long as the above version the first time is is run, but the second time takes a little less than 2 minutes.

We create the two `renv` environment variables  `RENV_PATHS_CACHE` and `RENV_PATHS_LIBRARY` and ensure the path is ignored when we check the package by adding is to the `.Rbuildignore` by the command `echo "^.renv$" >> .Rbuildignore`

```yaml
image: r-base:latest


stages:
  - check


variables:
  RENV_PATHS_CACHE: ${CI_PROJECT_DIR}/.renv/cache
  RENV_PATHS_LIBRARY: ${CI_PROJECT_DIR}/.renv/lib


cache:
  # Use a cache for each branch
  # For usage of keys, see https://docs.gitlab.com/ee/ci/caching/#sharing-caches-across-the-same-branch
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - ${RENV_PATHS_CACHE}
    - ${RENV_PATHS_LIBRARY}


check:
  stage: check
  script:
  - 'apt-get update'
  # Install needed packages. The call makes apt-get say yes to everything (-y),
  # and give no output except for errors (-qq)
  # We use the `check()` function of the devtools-package which imports the R-packages
  # curl - which need `libcurl4-openssl-dev` to be installed
  # openssl -  which need `libssl-dev` to be installed
  # xml2 - which need `libxml2-dev` to be installed
  - 'apt-get install -y -qq libcurl4-openssl-dev libssl-dev libxml2-dev'
  # Satisfy system dependencies for the R-packages
  # systemfonts (ggplot2 -> ragg -> systemfonts) needs libfontconfig1-dev
  # textshaping (ggplot2 -> ragg -> textshaping) needs libharfbuzz-dev libfribidi-dev
  # ragg needs libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
  # We need 'qpdf' for checks on pdfs
  - >
    apt-get install -y -qq
    libfontconfig1-dev
    libharfbuzz-dev libfribidi-dev
    libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
    qpdf
    # Restore renv
    - Rscript -e 'renv::restore()'
    # Make sure the paths are in .Rbuildignore
    - echo "^.renv$" >> .Rbuildignore
    # Install devtools just in caseit is not used in renv
    - Rscript -e 'install.packages("devtools")' -e 'devtools::check()'

```

Just for sports, we can refactor the above into:

```yaml
image: r-base:latest


stages:
  - setup
  - check


variables:
  RENV_PATHS_CACHE: ${CI_PROJECT_DIR}/.renv/cache
  RENV_PATHS_LIBRARY: ${CI_PROJECT_DIR}/.renv/lib


cache:
  # Use a cache for each branch
  # For usage of keys, see https://docs.gitlab.com/ee/ci/caching/#sharing-caches-across-the-same-branch
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - ${RENV_PATHS_CACHE}
    - ${RENV_PATHS_LIBRARY}


before_script:
  - 'apt-get update'
  # Install needed packages. The call makes apt-get say yes to everything (-y),
  # and give no output except for errors (-qq)
  # We use the `check()` function of the devtools-package which imports the R-packages
  # curl - which need `libcurl4-openssl-dev` to be installed
  # openssl -  which need `libssl-dev` to be installed
  # xml2 - which need `libxml2-dev` to be installed
  - 'apt-get install -y -qq libcurl4-openssl-dev libssl-dev libxml2-dev'
  # Satisfy system dependencies for the R-packages
  # systemfonts (ggplot2 -> ragg -> systemfonts) needs libfontconfig1-dev
  # textshaping (ggplot2 -> ragg -> textshaping) needs libharfbuzz-dev libfribidi-dev
  # ragg needs libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
  # We need 'qpdf' for checks on pdfs
  - >
    apt-get install -y -qq
    libfontconfig1-dev
    libharfbuzz-dev libfribidi-dev
    libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
    qpdf


setup:
  stage: setup
  script:
    # Restore renv
    - Rscript -e 'renv::restore()'
    # Install devtools just in case it is not used in renv
    - Rscript -e 'install.packages("devtools")'


check:
  stage: check
  dependencies:
    - setup
  when: on_success
  script:
    # Make sure the paths are in .Rbuildignore
    - echo "^.renv$" >> .Rbuildignore
    - Rscript -e 'devtools::check()'
```

Because Gitlab pulls the docker image on each stage, this solution is a little slower, but if the stages are complex it can be justified. Also, when the pipeline is separated into steps, the cache is created in the set-up-step (when successful) and remain available, should the check-step fail.

## Including artifacts

The Rcheck logs are lost in the current set-up - then the pipe line ends, the files are deleted. We can preserve the files as artifacts for later download and problem solving. I find the easiest way to do this is to make sure I know where the files are saved to, which can be done through setting the `check_dir` parameter of `devtools::check()`. The path we want to use is stored in the variable `CHECK_DIR` and could be something along the lines of  `"${CI_PROJECT_DIR}/logs"`. The log files we are interested in are stored in a directory called `[package name].Rcheck` and is a sub-directory to the path in `CHECK_DIR`. The path is stored in the variable `BUILD_LOGS_DIR` and could be `"${CI_PROJECT_DIR}/logs/${CI_PROJECT_TITLE}.Rcheck"`. It *might* be that you have to use the predefined CI variable `CI_PROJECT_NAME` rather than `CI_PROJECT_TITLE`.

```yaml
image: r-base:latest


stages:
  - setup
  - check


variables:
  RENV_PATHS_CACHE: ${CI_PROJECT_DIR}/.renv/cache
  RENV_PATHS_LIBRARY: ${CI_PROJECT_DIR}/.renv/lib
  CHECK_DIR: "${CI_PROJECT_DIR}/logs"
  # Some tutorials will mention the use of CI_PROJECT_NAME, but CI_PROJECT_TITLE is used in the path generation
  BUILD_LOGS_DIR: "${CI_PROJECT_DIR}/logs/${CI_PROJECT_TITLE}.Rcheck"


cache:
  # Use a cache for each branch
  # For usage of keys, see https://docs.gitlab.com/ee/ci/caching/#sharing-caches-across-the-same-branch
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - ${RENV_PATHS_CACHE}
    - ${RENV_PATHS_LIBRARY}


before_script:
  - 'apt-get update'
  # Install needed packages. The call makes apt-get say yes to everything (-y),
  # and give no output except for errors (-qq)
  # We use the `check()` function of the devtools-package which imports the R-packages
  # curl - which need `libcurl4-openssl-dev` to be installed
  # openssl -  which need `libssl-dev` to be installed
  # xml2 - which need `libxml2-dev` to be installed
  - 'apt-get install -y -qq libcurl4-openssl-dev libssl-dev libxml2-dev'
  # Satisfy system dependencies for the R-packages
  # systemfonts (ggplot2 -> ragg -> systemfonts) needs libfontconfig1-dev
  # textshaping (ggplot2 -> ragg -> textshaping) needs libharfbuzz-dev libfribidi-dev
  # ragg needs libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
  # We need 'qpdf' for checks on pdfs
  - >
    apt-get install -y -qq
    libfontconfig1-dev
    libharfbuzz-dev libfribidi-dev
    libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
    qpdf


setup:
  stage: setup
  script:
    # Restore renv
    - Rscript -e 'renv::restore()'
    # Install devtools and covr just in case they are not used in renv
    - Rscript -e 'install.packages(c("devtools", "covr"))'


check:
  stage: check
  dependencies:
    - setup
  when: on_success
  script:
    # Make sure the renv cache and lib paths are in .Rbuildignore
    - echo "^.renv$" >> .Rbuildignore
    - Rscript -e 'devtools::check(check_dir = Sys.getenv("CHECK_DIR"))'
  artifacts:
    paths:
      - $BUILD_LOGS_DIR/00install.out
      - $BUILD_LOGS_DIR/00check.log
    expire_in: 1 week
```

The artifacts from the check stage should be automatically deleted after 1 week, but can be made to last longer by modifying the `expire_in` value.

## Using coverage

To flaunt all the hard work we've put into documenting and testing the functionality of the package, we can include a coverage badge and let the R-package `covr` and in particular the function `gitlab()` do the work. The test coverage parsing pattern is `Coverage: \d+.\d+%$` with is included with the keyword `coverage` in the job.

The coverage is in its own stage here, because I find the separation logical.

```yaml
image: r-base:latest


stages:
  - setup
  - check
  - coverage


variables:
  RENV_PATHS_CACHE: ${CI_PROJECT_DIR}/.renv/cache
  RENV_PATHS_LIBRARY: ${CI_PROJECT_DIR}/.renv/lib
  CHECK_DIR: "${CI_PROJECT_DIR}/logs"
  # Some tutorials will mention the use of CI_PROJECT_NAME, but CI_PROJECT_TITLE is used in the path generation
  BUILD_LOGS_DIR: "${CI_PROJECT_DIR}/logs/${CI_PROJECT_TITLE}.Rcheck"


cache:
  # Use a cache for each branch
  # For usage of keys, see https://docs.gitlab.com/ee/ci/caching/#sharing-caches-across-the-same-branch
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - ${RENV_PATHS_CACHE}
    - ${RENV_PATHS_LIBRARY}


before_script:
  - 'apt-get update'
  # Install needed packages. The call makes apt-get say yes to everything (-y),
  # and give no output except for errors (-qq)
  # We use the `check()` function of the devtools-package which imports the R-packages
  # curl - which need `libcurl4-openssl-dev` to be installed
  # openssl -  which need `libssl-dev` to be installed
  # xml2 - which need `libxml2-dev` to be installed
  - 'apt-get install -y -qq libcurl4-openssl-dev libssl-dev libxml2-dev'
  # Satisfy system dependencies for the R-packages
  # systemfonts (ggplot2 -> ragg -> systemfonts) needs libfontconfig1-dev
  # textshaping (ggplot2 -> ragg -> textshaping) needs libharfbuzz-dev libfribidi-dev
  # ragg needs libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
  # We need 'qpdf' for checks on pdfs
  - >
    apt-get install -y -qq
    libfontconfig1-dev
    libharfbuzz-dev libfribidi-dev
    libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
    qpdf


setup:
  stage: setup
  script:
    # Restore renv
    - Rscript -e 'renv::restore()'
    # Install devtools and covr just in case they are not used in renv
    - Rscript -e 'install.packages(c("devtools", "covr"))'


check:
  stage: check
  dependencies:
    - setup
  when: on_success
  script:
    # Make sure the renv cache and lib paths are in .Rbuildignore
    - echo "^.renv$" >> .Rbuildignore
    - Rscript -e 'devtools::check(check_dir = Sys.getenv("CHECK_DIR"))'
  artifacts:
    paths:
      - $BUILD_LOGS_DIR/00install.out
      - $BUILD_LOGS_DIR/00check.log
    expire_in: 1 week


coverage:
  stage: coverage
  dependencies:
    - check
  when: on_success
  only:
    - master
  script:
   - Rscript -e 'covr::gitlab(type = c("tests", "examples"), quiet = FALSE)'
  coverage: '/Coverage: \d+\.\d+/'
  artifacts:
    paths:
      - public

```

### Adding the badge

The coverage badge is added in the section 'Badges' in 'General Settings'. In this project I use the following:

```
name: coverage report
link: https://gitlab.com/%{project_path}
Badge image URL: https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg
```

Similarly, the pipeline badge is added with:

```
name: pipeline status
link: https://gitlab.com/%{project_path}
Badge image URL: https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg
```

## A word on speed

The above configuration takes about 5 minutes to complete when all the dependencies are cached. By having just a single stage, we can reduce this to about 1-2 minutes - similar to what we saw after introduction of the cache.

```yaml
image: r-base:latest


stages:
  - all


variables:
  RENV_PATHS_CACHE: ${CI_PROJECT_DIR}/.renv/cache
  RENV_PATHS_LIBRARY: ${CI_PROJECT_DIR}/.renv/lib
  CHECK_DIR: "${CI_PROJECT_DIR}/logs"
  # Some tutorials will mention the use of CI_PROJECT_NAME, but CI_PROJECT_TITLE is used in the path generation
  BUILD_LOGS_DIR: "${CI_PROJECT_DIR}/logs/${CI_PROJECT_TITLE}.Rcheck"


cache:
  # Use a cache for each branch
  # For usage of keys, see https://docs.gitlab.com/ee/ci/caching/#sharing-caches-across-the-same-branch
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - ${RENV_PATHS_CACHE}
    - ${RENV_PATHS_LIBRARY}


before_script:
  - 'apt-get update'
  # Install needed packages. The call makes apt-get say yes to everything (-y),
  # and give no output except for errors (-qq)
  # We use the `check()` function of the devtools-package which imports the R-packages
  # curl - which need `libcurl4-openssl-dev` to be installed
  # openssl -  which need `libssl-dev` to be installed
  # xml2 - which need `libxml2-dev` to be installed
  - 'apt-get install -y -qq libcurl4-openssl-dev libssl-dev libxml2-dev'
  # Satisfy system dependencies for the R-packages
  # systemfonts (ggplot2 -> ragg -> systemfonts) needs libfontconfig1-dev
  # textshaping (ggplot2 -> ragg -> textshaping) needs libharfbuzz-dev libfribidi-dev
  # ragg needs libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
  # We need 'qpdf' for checks on pdfs
  - >
    apt-get install -y -qq
    libfontconfig1-dev
    libharfbuzz-dev libfribidi-dev
    libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
    qpdf


allInOne:
  stage: all
  script:
    # Restore renv
    - Rscript -e 'renv::restore()'
    # Install devtools and covr just in case they are not used in renv
    - Rscript -e 'install.packages(c("devtools", "covr"))'
    # Make sure the renv cache and lib paths are in .Rbuildignore
    - echo "^.renv$" >> .Rbuildignore
    - Rscript -e 'devtools::check(check_dir = Sys.getenv("CHECK_DIR"))'
    - Rscript -e 'covr::gitlab(type = c("tests", "examples"), quiet = FALSE)'
  artifacts:
    paths:
      - $BUILD_LOGS_DIR/00install.out
      - $BUILD_LOGS_DIR/00check.log
      - public
  coverage: '/Coverage: \d+\.\d+/'

```

## `gitlab-ci-local`

I recently came across the tool [`gitlab-ci-local`](https://github.com/firecow/gitlab-ci-local) which makes it easy to run the gitlab CI locally - a great way to get the `.gitlab-ci.yml` working and run tests before pushing to gitlab.

It does have some drawbacks though. One is the default placement of artifacts in the project-directory, but a much larger is the renv-library. The problem here is that renv/lib gets filled with symbolic links pointing to nothing, which in turns make `devtools::check()` fail. The actual reason is that `file.copy()` chokes when copying the files to prepare for the packaging of the package - `file.copy()` is called by `pkgbuild::build()`, which in turn is called from within `devtools::check()`.

The solution to the first problem is simple: run `gitlab-ci-local` with the flag `--no-artifacts-to-source`. It is also possible to use the '@NoArtifactsToSource' decorator for each relevant job, but I prefer:

```
gitlab-ci-local --no-artifacts-to-source [stage]
```

The second problem is also easy - just don't cache the renv/lib. However, this means that the renv must be restored before each stage, making the whole setup-stage irrelevant. Time wise there is only a little loss when the installed packages are linked from renv/cache to renv/lib. The modified `.gitlab-ci.yml` makes everyone happy:

```yaml
image: r-base:latest


stages:
  # - setup
  - check
  - coverage


variables:
  RENV_DIR: "ci_renv"
  RENV_PATHS_CACHE: ${CI_PROJECT_DIR}/${RENV_DIR}/cache
  RENV_PATHS_LIBRARY: ${CI_PROJECT_DIR}/${RENV_DIR}/lib
  CHECK_DIR: "${CI_PROJECT_DIR}/logs"
  # Some tutorials will mention the use of CI_PROJECT_NAME, but CI_PROJECT_TITLE is used in the path generation
  BUILD_LOGS_DIR: "${CI_PROJECT_DIR}/logs/${CI_PROJECT_TITLE}.Rcheck"


cache:
  # Use a cache for each branch
  # For usage of keys, see https://docs.gitlab.com/ee/ci/caching/#sharing-caches-across-the-same-branch
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - ${RENV_PATHS_CACHE}
    # The library path is not strictly needed, as it contains symlinks to the cache
    # Additionally, the combination of gitlab-ci-local and symlinks will make local
    # package building choke (file.copy complains there is no file to copy)
    #
    # The disadvantage is that we must restore the environment at each stage
    # - ${RENV_PATHS_LIBRARY}


before_script:
  - 'apt-get update'
  # Install needed packages. The call makes apt-get say yes to everything (-y),
  # and give no output except for errors (-qq)
  # We use the `check()` function of the devtools-package which imports the R-packages
  # curl - which need `libcurl4-openssl-dev` to be installed
  # openssl -  which need `libssl-dev` to be installed
  # xml2 - which need `libxml2-dev` to be installed
  - 'apt-get install -y -qq libcurl4-openssl-dev libssl-dev libxml2-dev'
  # Satisfy system dependencies for the R-packages
  # systemfonts (ggplot2 -> ragg -> systemfonts) needs libfontconfig1-dev
  # textshaping (ggplot2 -> ragg -> textshaping) needs libharfbuzz-dev libfribidi-dev
  # ragg needs libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
  # We need 'qpdf' for checks on pdfs
  - >
    apt-get install -y -qq
    libfontconfig1-dev
    libharfbuzz-dev libfribidi-dev
    libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
    qpdf
  - ""
  # The renv lib-path is not cached between stages, we need to make sure everyting
  # is restored
  - Rscript -e 'install.packages("renv")'
  - Rscript -e 'renv::restore()'


# setup:
#   stage: setup
#   script:
#     - echo "This is not seen"


check:
  stage: check
  # dependencies:
  #   - setup
  # when: on_success
  script:
    # Make sure the renv cache and lib paths are in .Rbuildignore
    - echo "^${RENV_DIR}$" >> .Rbuildignore
    - Rscript -e 'renv::install(c("devtools"))'
    - Rscript -e 'devtools::check(check_dir = Sys.getenv("CHECK_DIR"))'
  artifacts:
    paths:
      - $BUILD_LOGS_DIR/00install.out
      - $BUILD_LOGS_DIR/00check.log
    expire_in: 1 week


coverage:
  stage: coverage
  allow_failure: true
  dependencies:
    - check
  when: on_success
  only:
    - master
  script:
    - Rscript -e 'renv::install(c("covr", "DT"))'
    - Rscript -e 'covr::gitlab(type = c("tests", "examples"), quiet = FALSE)'
  coverage: '/Coverage: \d+\.\d+/'
  artifacts:
    paths:
      - public
```

## Other ideas

Gitlab provides a container registry, which could be used to build a docker image on a regular basis using the 'Schedule' functionality. More information can be found here at [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html) and [Pipeline schedules](https://docs.gitlab.com/ee/ci/pipelines/schedules.html).
